import numpy as np
import matplotlib.pyplot as plt 

fname=open(r"C:\Users\student\rusu-lv-2022-23\LV2\resources\mtcars.csv")
l=[] 
for line in fname:
    line=line.split(",")
    l.append(line)
index_mpg=l[0].index('"mpg"')
index_hp=l[0].index('"hp"')
index_wt=l[0].index('"wt"')
mpg=[]
hp=[]
wt=[]
for i in range (1,len(l)):
    mpg.append(float(l[i][index_mpg]))
    hp.append(float(l[i][index_hp]))
    wt.append(float(l[i][index_wt]))
print(mpg)
print(hp)
mpg_array=np.array(mpg)
hp_array=np.array(hp)
wt_array=np.array(wt)
colors = np.random.rand(3)
a=plt.scatter(mpg_array, hp_array,c='b',label='mpg-hp')
a1=plt.scatter(mpg_array, wt_array,c='c',label='mpg-wt')
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=4)
plt.show()
print("min",mpg_array.min())
print("max",mpg_array.max())
print("sr",round(mpg_array.mean(),2))

