import numpy as np
import matplotlib.pyplot as plt
throws = np.random.choice([1,2,3,4,5,6], size=(100))
plt.hist(throws)
plt.show()
