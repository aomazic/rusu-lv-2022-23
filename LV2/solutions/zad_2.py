import re

try:
    fhand = open(r"C:\Users\student\rusu-lv-2022-23\LV2\resources\mbox-short.txt")
except:
    print ('File cannot be opened:', r'C:\Users\student\rusu-lv-2022-23\LV2\resources\mbox-short.txt')
    exit()
emails = []
text = fhand.read()
# 1. sadrži najmanje jedno slovo a 
emails = re.findall('([a-zA-Z.]*a+[A-zA-Z.]*)@[a-zA-Z.]+', text)
print(emails)
# 2. sadrži točno jedno slovo a
emails = re.findall('([b-zB-Z.]*a[b-zB-Z.]*)@[a-zA-Z.]+', text)
print(emails)
# 3. ne sadrži slovo a 
emails = re.findall('([b-zB-Z.]+)@[a-zA-Z.]+', text)
print(emails)
# 4. sadrži jedan ili više numeričkih znakova (0 – 9) 
emails = re.findall('([a-zA-Z.0-9]*[0-9]+[a-zA-Z.0-9]*)@[a-zA-Z.]+', text)
print(emails)
# 5. sadrži samo mala slova (a-z)
emails = re.findall('([a-z.]+)@[a-zA-Z.]+', text)
print(emails)