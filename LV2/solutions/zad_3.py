import numpy as np
import matplotlib.pyplot as plt 

v1 = np.random.choice([0,1], size=(10,))
print(v1)

v2 = np.ones(10)

for i in range(0,10):
    if(v1[i] == 1):
        v2[i] = np.random.normal(180,7)
    else:
        v2[i] = np.random.normal(167,7)

mf = np.stack((v1,v2), axis=1)
mf = mf[mf[:, 0].argsort()]
mf2 = np.split(mf[:, 1], np.unique(mf[:, 0], return_index=True)[1][1:])
plt.figure(1)
plt.plot(mf2[0],'r')
plt.plot(mf2[1],'b')
plt.plot(mf2[0].mean(),'bo',color='red')
plt.plot(mf2[1].mean(),'bo' ,color='blue')
plt.show()



