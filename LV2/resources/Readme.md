### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset

lv2_zad1: ispravni e-mailovi su bili provjereni pomoću regularnog izraza ([a-zA-Z.]+)@[a-zA-Z.]+ gdje su () odvojile samo prvi dio ispravnih mailova
lv2_zad2: za različite slućajeve korišteni su različini regularni izrazi
lv2_zad3: pomoću stack metode vrijednosti visina su pripojene vrijednostima 0,1 i pomoću split su vrijednosti zasebno nacrtane na graf
lv2_zad4: s random.choice generirano je 100 brojeva u rasponu od 1-6 i s plt.hist su vrijedonsti prikazane na histografu
lv2_zad5: svaka linija datoteke je rastavljena na array string vrijednosti gdje preko određenog indexa su dobivene tražene vrijednosti koje su se dalje prikazale na grafu
lv2_zad6: množenjem r,g,b vrijednosti s koeficjentom 1.5 povećala se svjetlina 