import urllib.request as urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

#1
# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=02.01.2017&vrijemeDo=12.12.2017'
airQualityHR = urllib.urlopen(url).read()
root = ET.fromstring(airQualityHR)
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))
children = list(root)
i = 0
while True:
    try:
        obj = list(children[i])
    except:
        break
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] =  pd.to_datetime(df['vrijeme'], utc=True).dt.month
df['dayOfweek'] =  pd.to_datetime(df['vrijeme'], utc=True).dt.dayofweek
df['year'] = pd.to_datetime(df['vrijeme'], utc=True).dt.year
print(df)

#2
print("Datumi s najvecom koncentracijom PM10:")
print(df.sort_values(by=['mjerenje'], ascending=False).head(3)['vrijeme'])

#3
df.groupby('month').mjerenje.count().plot(kind='bar')
plt.ylabel('Broj mjerenja')

#4
df[(df['month'] == 7) | (df['month'] == 1)].groupby(by='month').boxplot(by='month',column='mjerenje')

#5
df.loc[df['dayOfweek'] >= 5, 'weekend'] = True
df.loc[df['dayOfweek'] < 5, 'weekend'] = False
print(df)
 
df[(df['weekend'] == True) | (df['weekend'] == False)].boxplot(by='weekend',column='mjerenje')
plt.show()
