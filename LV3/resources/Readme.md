### mtcars.csv

Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models)
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset



### AirQualityRH.py

Skripta za dohvaćanje podataka o kvaliteti zraka pomocu REST API

Zadatak1: pomoću pandasa se učitaju vrijednosti iz csv datoteke u pandas data frame objekt i radilo se dodavanje novog stupca, izdvajanje pojedinog stupca ili određenih redova iz DataFramea itd..

Zadatak2: korištenjem pandasa i matplotlib.plot visualno su se prikazali podaci iz csv na različite načine

Zadatak3: Korištenjem  RESTfull servisa dohvaćeni su podatci s interneta i dalje su obrađeni pomoću pandasa i matplot liba