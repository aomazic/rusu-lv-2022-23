import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv(r'C:\Users\student\aomazicRusu\rusu-lv-2022-23\LV3\resources\mtcars.csv')
#1
mtcars.sort_values(by='cyl').plot(x="cyl", y='mpg', kind="bar")
plt.show()
 
#2
mtcars.sort_values(by='cyl').boxplot(by='cyl',column='mpg')
plt.show()
 
#3
mtcars_am = mtcars.groupby('am')['mpg'].mean().plot(kind='bar')
print(mtcars_am)
print('Automobili s automatskim mjenjacem imaju vecu potrosnju')
plt.show()

#4
mtcars['qsec/hp'] = mtcars['qsec'] / mtcars['hp']
mtcars.groupby('am')['qsec/hp'].mean().plot(kind='bar')
plt.show()