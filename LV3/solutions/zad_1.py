import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
mtcars = pd.read_csv(r'C:\Users\student\aomazicRusu\rusu-lv-2022-23\LV3\resources\mtcars.csv')

#1
sorted_mtcars = mtcars.sort_values(by = ["mpg"])
print(sorted_mtcars.head(5))

#2
print(sorted_mtcars[(sorted_mtcars.cyl == 8)].tail(3)) 

#3
new_mtcars = mtcars.groupby('cyl')
print("srednja potrošnja automobila sa 6 cilindara:" ,new_mtcars.mean().iloc[1,0]) 

#4
sorted_mtcars = mtcars[(mtcars.cyl == 4)  & (mtcars['wt'].between(2.0,2.2))]
print("srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs:",sorted_mtcars.groupby('cyl').mean().iloc[0,0])

#5 
print("broj automobila s automatskim mjenjačem" , mtcars[(mtcars.am == 1)].groupby('am').count().iloc[0,0]) 
print("broj automobila s ručnim mjenjačem:" , mtcars[(mtcars.am == 0)].groupby('am').count().iloc[0,0]) 

#6 
print("broj automobila s ručnim mjenjačem i snagom preko 100 konjskih snaga:",mtcars[(mtcars.am == 1) & (mtcars.hp > 100)].groupby('am').count().iloc[0,0]) 

#7 
mtcars['kg'] = mtcars.wt * 0.4536 * 1000
print(mtcars)

