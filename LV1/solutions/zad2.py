try:
    ocjena = float(input('unesi ocjenu: '))
except:
    print ('Ocjena nije broj')
    exit()
if ocjena > 1 or ocjena < 0:
        ocjena = float(input('ocjena mora biti između 0.0 i 1.0, unesite ponovno: '))
if ocjena >= 0.9:
    print('A')
elif ocjena >= 0.8:
    print('B')
elif ocjena >= 0.7:
    print('C')
elif ocjena >= 0.6:
    print('D')
elif ocjena < 0.6:
    print('F')    