fname = input('Enter the file name: ')
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

host_names = []
adrese = []
for line in fhand:
    line = line.rstrip()
    if line.startswith('From'):
        text = line[5:].split()
        adrese.append(text[0])

for adresa in adrese:
    text = adresa.split('@')
    host_names.append(text[1])

counts = dict()
for host_name in host_names:
        if host_name not in counts:
            counts[host_name] = 1
        else:
            counts[host_name] += 1

print(counts)
