Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.
Promjene:
- Print dodane zagrade
- raw_input izmjenjen u input
- fhand = open(fnamex) izmjenjeno u fhand = open(fname)

Vježba prikzuje osnove korištenja gita i osnove pythona
- prikazuje kako klonirati git repozitoriji lokalno te kako spremiti promjene lokalno i remote
- prikazuje kako s pythono raditi input te osnove rada s stringovima, listama i dictionary-om